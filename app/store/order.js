import Vue from 'nativescript-vue';
import Vuex from 'vuex';
Vue.use(Vuex);

 const order = new Vuex.Store({
    state: {
            engines: 50,
            weapons: 50
    },
    mutations: {
        increaseEngines(state) {
            if (state.engines < 100) {
                state.engines++;
                state.weapons--;
            }
        },
        decreaseEngines(state) {
            if (state.engines > 0) {
                state.engines--;
                state.weapons++;
            }
        },
        increaseWeapons(state) {
            if (state.weapons < 100) {
                state.weapons++;
                state.engines--;
            }
        },
        decreaseWeapons(state) {
            if (state.weapons > 0) {
                state.weapons--;
                state.engines++;
            }
        }
    }
});
module.exports = order;
