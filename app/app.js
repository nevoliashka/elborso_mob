import * as ApplicationSettings from "@nativescript/core/application-settings";
import * as application from '@nativescript/core/application';
//const application = require("tns-core-modules/application");
//import * as SocialShare from "nativescript-social-share";
import Vue from 'nativescript-vue'
import Vuex from 'vuex';
Vue.use(Vuex);

const order = new Vuex.Store({
   state: {
           name: '',
           phone: '',
           adress: '',
           list: [],
           wishlist:[],
           sum: 0,
           remember: false,
           searchwords:[],
           token:''
   },
   mutations: {

     addToken(state, ord) {
       console.log(ord)
      state.token=ord

      ApplicationSettings.setString("token", JSON.stringify(state.token));
     },
     removeToken(state) {
      state.token=''
      ApplicationSettings.remove("token");
     },
   addSearch(state, ord) {

       if(state.searchwords.length>8){
          state.searchwords.splice(8,1)
          console.log(state.searchwords+' d'+state.searchwords.light)
         state.searchwords.unshift(ord.val)

       }else{

         state.searchwords.unshift(ord.val)
       }

       ApplicationSettings.setString("searchwords", JSON.stringify(state.searchwords));
      //  state.searchwords=[]
    },
       add(state, ord) {

         let search = ord.id;
         let cityId = state.list.findIndex(city => city.id === search)

         if(cityId!=-1){
         //    console.log(cityId)
           state.list[cityId]['count']+=1
           state.sum+=ord.price
         }else{
           state.list.push(ord)
           state.sum+=ord.price
         }

    //  console.log(state.list)
         ApplicationSettings.setString("list", JSON.stringify(state.list));
         ApplicationSettings.setString("sum", JSON.stringify(state.sum));
       },

       remember(state){
         ApplicationSettings.setString("remember", JSON.stringify(true));
         ApplicationSettings.setString("adress", JSON.stringify(state.adress));
        //  console.log('ss'+state)
       ApplicationSettings.setString("name", JSON.stringify(state.name));
  ApplicationSettings.setString("phone", JSON.stringify(state.phone));
 console.log(state)

      },
      clearCart(state) {
         state.list=[]
         state.sum=0
         ApplicationSettings.remove("list");
         ApplicationSettings.remove("sum");
      },
       remove(state,  { todo } ) {

         let arr=state.list
           state.sum=0

         for (let index = 0; index < arr.length; index++) {
            if (arr[index].id == todo){arr.splice(index, 1)}
             }

         for (let index = 0; index < arr.length; index++) {
            if (arr[index].id == todo){arr.splice(index, 1)}
               state.sum+=(arr[index].price*arr[index].count);
             }

             ApplicationSettings.setString("list", JSON.stringify(arr));
             ApplicationSettings.setString("sum", JSON.stringify(state.sum));
       },

       addCount(state, ord){
      //   console.log(ord.count)
         state.list[ord.index]['count']=ord.count

     let arr=state.list
       state.sum=0
     for (let index = 0; index < arr.length; index++) {
           state.sum+=(arr[index].price*arr[index].count);

         }
         ApplicationSettings.setString("list", JSON.stringify(arr));
         ApplicationSettings.setString("sum", JSON.stringify(state.sum));
         //  state.list[ord.index]["count"]=ord.count
       },

       //избранное
       addWishlist(state, ord) {
         let search = ord.id;
         let cityId = state.wishlist.findIndex(city => city.id === search)
         if(cityId==-1){
           state.wishlist.push(ord)
            ApplicationSettings.setString("wishlist", JSON.stringify(state.wishlist));
         }
       },
       removeWishlist(state,  { todo } ) {
         console.log(todo)
         let arr=state.wishlist

         for (let index = 0; index < arr.length; index++) {
            if (arr[index].id == todo){arr.splice(index, 1)}
             }

             ApplicationSettings.setString("wishlist", JSON.stringify(arr));

       },

       loadFromStorage(state) {
  //       ApplicationSettings.clear();
      const storedState = ApplicationSettings.getString("list");
      const storedSum = ApplicationSettings.getString("sum");
      const storedName = ApplicationSettings.getString("name");
      const storedPhone = ApplicationSettings.getString("phone");
      const storedAdress = ApplicationSettings.getString("adress");
      const storedRemember = ApplicationSettings.getString("remember");
      const storedWishlist = ApplicationSettings.getString("wishlist");
      const storedSearchwords = ApplicationSettings.getString("searchwords");
      const storedToken = ApplicationSettings.getString("token");


      if(storedToken) {
          const statetoken = JSON.parse(storedToken);
          state.token = statetoken;
            }


      if(storedSearchwords) {
          const statesearchwords = JSON.parse(storedSearchwords);
          state.searchwords = statesearchwords;
            }

          if(storedRemember) {
              const stateremember = JSON.parse(storedRemember);
              state.remember = stateremember;
                }
          if(storedState) {
              const stateload = JSON.parse(storedState);
              state.list = stateload;
                }
            if(storedSum){
              const statesum = JSON.parse(storedSum);
              state.sum = statesum;
            }
            if(storedName){
                const statename = JSON.parse(storedName);
                state.name = statename;
                console.log(statename)
            }
            if(storedPhone){
                const statephone = JSON.parse(storedPhone);
                  state.phone = statephone;
            }
              if(storedAdress){
                  const stateadress = JSON.parse(storedAdress);
                  state.adress = stateadress;
              }
              if(storedWishlist) {
                  const statewishlist = JSON.parse(storedWishlist);
                  state.wishlist = statewishlist;
                    }
        },

        addAdress(state, adress) {
                state.adress=adress.adress;
        },
        addDatacliet(state,client){
//console.log(client.client.name)
          state.name=client.client.name;
          state.phone=client.client.phone;
          //  console.log(state)
        }
   }
});

import CanvasSVG from '@nativescript-community/ui-svg/vue';
Vue.use(CanvasSVG);

import {TNSFontIcon, fonticon} from 'nativescript-fonticon';

TNSFontIcon.debug = true;
TNSFontIcon.paths = {
   'mdi': 'material-design-icons.css'
};
TNSFontIcon.loadCss();
application.setResources( { fonticon } );

import { CheckBox } from '@nstudio/nativescript-checkbox';
Vue.registerElement(
  'CheckBox',
  () => CheckBox,
  {
    model: {
      prop: 'checked',
      event: 'checkedChange'
    }
  }
);

//import * as SocialShare from "nativescript-social-share";


import Home from './components/Home'

Vue.registerElement('Carousel', () => require('nativescript-carousel').Carousel);
Vue.registerElement('CarouselItem', () => require('nativescript-carousel').CarouselItem);
//Vue.use( axios)

new Vue({
  render: (h) => h('frame', [h(Home)]),
  store: order

}).$start()
